﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLogic
{
    public class Lair
    {
        BaseLevel level;
        private double loot_amount;
        Project upgrade_project;
        MainCode.Command victory_command;
        MainCode.Command time_command;
        private int victory_loot;
        private Player player_obj;

        public double Loot_amount { get => loot_amount; set => loot_amount = value; }
        public BaseLevel Level => level;

        public Lair(BaseLevel current_level,
            MainCode.Command base_victory_command,
            MainCode.Command base_time_command,
            Player base_player_obj)
        {
            level = current_level;
            loot_amount = 0;
            MainCode.Command upgrade_command = new MainCode.Command(upgrade);
            upgrade_project = new Project(
                "Upgrade Lair",
                level.Next_level.Size,
                upgrade_command);
            victory_command = base_victory_command;
            time_command = base_time_command;
            victory_loot = 40;
            player_obj = base_player_obj;
        }

        public int size()
        {
            int current_size = level.Size;
            return current_size;
        }

        public void remove_loot()
        {
            loot_amount = loot_amount - 1;
            if (loot_amount < 0)
            {
                loot_amount = 0;
            }
        }

        public void add_loot(double added_loot)
        {
            loot_amount = loot_amount + added_loot;
        }

        public SpriteBatch draw(SpriteBatch spriteBatch,
            SpriteFont font)
        {
            if (loot_amount >= victory_loot)
            {
                Victory();
            }

            string loot_text = "Loot: " + loot_amount.ToString();
            Vector2 loot_coords = new Vector2(10, 110);
            spriteBatch.DrawString(
                font,
                loot_text,
                loot_coords,
                Color.White);

            spriteBatch = level.draw(spriteBatch, font);

            if (upgrade_project != null)
            {
                spriteBatch = upgrade_project.draw(spriteBatch, font);
            }

            double total_used_storage = loot_amount;
            string used_storage_text = total_used_storage.ToString();
            string storage_text = "Storage: " + used_storage_text + "/" + level.Size.ToString();
            Vector2 storage_text_pos = new Vector2(10, 160);
            spriteBatch.DrawString(
                font,
                storage_text,
                storage_text_pos,
                Color.White);

            string victory_text = "Goal: " + "Collect " + victory_loot.ToString() + " loot!";
            Vector2 victory_text_pos = new Vector2(700, 10);
            spriteBatch.DrawString(
                font,
                victory_text,
                victory_text_pos,
                Color.White);

            return spriteBatch;
        }

        public void upgrade()
        {
            if (level.Next_level != level)
            {
                level = level.Next_level;
                reset_project();
            }
        }

        public void update()
        {

        }

        public void reset_project()
        {
            if (level.Next_level != level)
            {
                MainCode.Command upgrade_command = new MainCode.Command(upgrade);
                Project new_project = new Project(
                    "Upgrade Lair",
                    level.Next_level.Size,
                    upgrade_command);
                upgrade_project = new_project;
            }
            else
            {
                upgrade_project = null;
            }
        }

        public void Victory()
        {
            victory_command.execute();
        }

        public bool Is_full()
        {
            bool result = false;

            if (loot_amount > level.Size)
            {
                result = true;
            }

            return result;
        }

        public void upgrade_action()
        {
            if (upgrade_project != null)
            {
                upgrade_project.update(player_obj.GetNomMod());
                time_command.execute();
            }
        }
    }

    public class BaseLevel
    {
        readonly int size;
        readonly string name;
        readonly BaseLevel next_level;
        readonly Texture2D image;
        readonly Vector2 image_pos;

        public BaseLevel(
            int level_size,
            string level_name,
            Texture2D level_image,
            BaseLevel level_next = null)
        {
            size = level_size;
            name = level_name;
            image = level_image;
            if (level_next == null)
            {
                next_level = this;
            }
            else
            {
                next_level = level_next;
            }
            image_pos = new Vector2(460, 200);
        }

        public int Size => size;

        public string Name => name;

        public BaseLevel Next_level => next_level;

        public SpriteBatch draw(
            SpriteBatch spriteBatch,
            SpriteFont font)
        {
            string text = name;
            Vector2 pos = new Vector2(700, 60);
            spriteBatch.DrawString(
                font,
                text,
                pos,
                Color.White);

            spriteBatch.Draw(
                image,
                image_pos,
                Color.White);

            return spriteBatch;
        }
    }

    public class Project
    {
        private string name;
        private double labor_required;
        private double labor_invested;
        private MainCode.Command result_func;

        public Project(
            string project_name,
            int project_labor_required,
            MainCode.Command project_result_func)
        {
            name = project_name;
            labor_required = project_labor_required;
            labor_invested = 0;
            result_func = project_result_func;
        }

        public void update(double mod)
        {
            double added_labor = 1 * mod;
            labor_invested = labor_invested + added_labor;

            if (labor_invested >= labor_required)
            {
                finish();
            }

        }

        private void finish()
        {
            result_func.execute();
        }

        public SpriteBatch draw(
            SpriteBatch spriteBatch,
            SpriteFont font)
        {
            string project_text = name;
            project_text = project_text + " : ";
            project_text = project_text + labor_invested.ToString();
            project_text = project_text + "/";
            project_text = project_text + labor_required.ToString();

            Vector2 text_pos = new Vector2(700, 110);
            spriteBatch.DrawString(
                font,
                project_text,
                text_pos,
                Color.White);

            return spriteBatch;
        }
    }

    public class Outside
    {
        private double collected_loot;
        private MainCode.Command time_command;
        private Texture2D background_img;
        private MainCode.Command add_food_command;
        private Player player_obj;

        public Outside(MainCode.Command outside_time_command, Texture2D outside_background_img, MainCode.Command outside_add_food_command, Player outside_player)
        {
            collected_loot = 0;
            time_command = outside_time_command;
            background_img = outside_background_img;
            add_food_command = outside_add_food_command;
            player_obj = outside_player;
        }

        public void Hunt()
        {
            add_food_command.execute();
            time_command.execute();
        }

        public double Collected_loot { get => collected_loot; set => collected_loot = value; }

        public SpriteBatch Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            spriteBatch.Draw(
                background_img,
                new Vector2(0, 0),
                Color.White);

            string loot_text = "Collected Loot: " + collected_loot.ToString();
            spriteBatch.DrawString(
                font,
                loot_text,
                new Vector2(1000, 300),
                Color.White);

            return spriteBatch;
        }

        public void Loot()
        {
            double loot_found = 1 * player_obj.GetNomMod();
            collected_loot = collected_loot + loot_found;
            time_command.execute();
        }

        public double Take_Loot()
        {
            double loot = collected_loot;
            collected_loot = 0;
            return loot;
        }
    }

    public class Player
    {
        private MainCode.Command game_over_command;
        public enum NomLevels
        {
            Dead,
            Starving,
            Hungry,
            Fed,
            Full,
            Gorged
        }
        private NomLevels current_nom_level;
        private double nom_counter;

        public NomLevels Current_nom_level => current_nom_level;

        public Player(MainCode.Command game_over = null)
        {
            game_over_command = game_over;
            current_nom_level = NomLevels.Fed;
            nom_counter = 9;
        }

        public void AddFood()
        {
            IncreaseNom();
        }

        public void EatFood()
        {
            nom_counter = nom_counter - 1;
            if (nom_counter <= 0)
            {
                DecreaseNom();
            }
        }

        public void AdvanceTime()
        {
            EatFood();
        }

        public void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            string text = "Nom: " + current_nom_level.ToString();

            spriteBatch.DrawString(
                font,
                text,
                new Vector2(10, 300),
                Color.White);

            string text_2 = "Nom Duration: " + nom_counter.ToString();

            spriteBatch.DrawString(
                font,
                text_2,
                new Vector2(10, 350),
                Color.White);

            string text_3 = "Nom Modifier: " + GetNomMod().ToString();

            spriteBatch.DrawString(
                font,
                text_3,
                new Vector2(10, 400),
                Color.White);
        }

        private void IncreaseNom()
        {
            switch (current_nom_level)
            {
                case NomLevels.Starving:
                    current_nom_level = NomLevels.Hungry;
                    nom_counter = 21;
                    break;
                case NomLevels.Hungry:
                    current_nom_level = NomLevels.Fed;
                    nom_counter = 13;
                    break;
                case NomLevels.Fed:
                    current_nom_level = NomLevels.Full;
                    nom_counter = 8;
                    break;
                case NomLevels.Full:
                    current_nom_level = NomLevels.Gorged;
                    nom_counter = 5;
                    break;
                case NomLevels.Gorged:
                    nom_counter = 4;
                    break;

            }
        }

        private void DecreaseNom()
        {
            switch (current_nom_level)
            {
                case NomLevels.Gorged:
                    current_nom_level = NomLevels.Full;
                    nom_counter = 5;
                    break;
                case NomLevels.Full:
                    current_nom_level = NomLevels.Fed;
                    nom_counter = 8;
                    break;
                case NomLevels.Fed:
                    current_nom_level = NomLevels.Hungry;
                    nom_counter = 13;
                    break;
                case NomLevels.Hungry:
                    current_nom_level = NomLevels.Starving;
                    nom_counter = 21;
                    break;
                case NomLevels.Starving:
                    current_nom_level = NomLevels.Dead;
                    nom_counter = 999999999999999;
                    break;
            }
        }

        public double GetNomMod()
        {
            switch (current_nom_level)
            {
                default: //for the Starving Nom level
                    return 0.2;
                case NomLevels.Hungry:
                    return 0.6;
                case NomLevels.Fed:
                    return 1;
                case NomLevels.Full:
                    return 1.4;
                case NomLevels.Gorged:
                    return 1.8;
            }
        }
    }
}