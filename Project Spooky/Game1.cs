﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace MainCode
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        SpriteFont font_40;
        SpriteFont font_30;

        Texture2D background_img;
        GameLogic.Outside outside_obj;
        Button upgrade_button;
        Button goLair_button;
        Texture2D outside_img;
        Texture2D storage_img;

        //BUTTON TEXTURES
        Texture2D button_texture;
        Texture2D button_hover_texture;
        Texture2D button_pressed_texture;
        Texture2D button_inactive_texture;

        //LEVEL textures
        Texture2D level_1_texture;
        Texture2D level_2_texture;
        Texture2D level_3_texture;
        Texture2D level_4_texture;
        Texture2D level_5_texture;
        Texture2D level_6_texture;
        Texture2D level_7_texture;
        Texture2D level_8_texture;
        Texture2D level_9_texture;

        int elapsed_days;

        Button hunt_button;
        Button goOut_button;
        Button throwOutLoot_button;

        MouseHelper mouse_helper;

        Key escape_key;
        
        enum ScreenType
        {
            Victory,
            Lost,
            Lair,
            Outside,
            Storage
        }

        ScreenType current_screen = ScreenType.Lair;

        GameLogic.Lair lair_obj;
        private Button loot_button;
        GameLogic.Player player_obj;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            Command hunt_command;
            Vector2 hunt_button_position;
            Vector2 upgrade_button_position;
            Command upgrade_command;
            Command exit_command;

            graphics.ToggleFullScreen();
            graphics.PreferredBackBufferHeight = 1080;
            graphics.PreferredBackBufferWidth = 1920;
            graphics.ApplyChanges();

            IsMouseVisible = true;

            mouse_helper = new MouseHelper();

            exit_command = new Command(Exit);
            escape_key = new Key(Keys.Escape, exit_command);

            elapsed_days = 1;

            base.Initialize();

            player_obj = new GameLogic.Player();

            Command advance_time = new Command(Advance_time);

            Command add_food = new Command(player_obj.AddFood);
            outside_obj = new GameLogic.Outside(advance_time, outside_img, add_food, player_obj);

            Vector2 button_size = new Vector2(160, 50);
            ButtonAssets button_assets = new ButtonAssets(
                button_texture, 
                button_hover_texture, 
                button_pressed_texture, 
                button_inactive_texture, 
                button_size,
                font_30);

            Command goOut_command = new Command(Go_outside);
            goOut_button = new Button(
                new Vector2(500, 800),
                goOut_command,
                "LEAVE",
                button_assets);

            Command goLair_commmand = new Command(Go_Lair);
            goLair_button = new Button(
                new Vector2(750, 600),
                goLair_commmand,
                "LAIR",
                button_assets);

            hunt_button_position = new Vector2(500, 400);
            string hunt_button_name = "HUNT";
            hunt_command = new Command(outside_obj.Hunt);
            hunt_button = new Button(
                hunt_button_position,
                hunt_command,
                hunt_button_name,
                button_assets);

            Vector2 loot_button_pos = new Vector2(1000, 400);
            string loot_button_name = "LOOT";
            Command loot_command = new Command(outside_obj.Loot);
            loot_button = new Button(
                loot_button_pos,
                loot_command,
                loot_button_name,
                button_assets);

            GameLogic.BaseLevel level_9 = new GameLogic.BaseLevel(45, "Grotto Suite (Lvl.9)", level_9_texture);
            GameLogic.BaseLevel level_8 = new GameLogic.BaseLevel(40, "Cavern (Lvl.8)", level_8_texture, level_9);
            GameLogic.BaseLevel level_7 = new GameLogic.BaseLevel(35, "Man-cave (Lvl.7)", level_7_texture, level_8);
            GameLogic.BaseLevel level_6 = new GameLogic.BaseLevel(30, "My Retreat (Lvl.6)", level_6_texture, level_7);
            GameLogic.BaseLevel level_5 = new GameLogic.BaseLevel(25, "Comfty Den (Lvl.5)", level_5_texture, level_6);
            GameLogic.BaseLevel level_4 = new GameLogic.BaseLevel(20, "Howling Hollow (Lvl.4)", level_4_texture, level_5);
            GameLogic.BaseLevel level_3 = new GameLogic.BaseLevel(15, "Chilly Cavity (Lvl.3)", level_3_texture, level_4);
            GameLogic.BaseLevel level_2 = new GameLogic.BaseLevel(10, "Hidey Hole (Lvl.2)", level_2_texture, level_3);
            GameLogic.BaseLevel level_1 = new GameLogic.BaseLevel(5, "Beggar's Nook (Lvl.1)", level_1_texture, level_2);

            Command victory_command = new Command(Win_game);
            lair_obj = new GameLogic.Lair(level_1, victory_command, advance_time, player_obj);

            //UPGRADE button
            upgrade_button_position = new Vector2(1300, 800);
            upgrade_command = new Command(lair_obj.upgrade_action);
            string upgrade_button_name = "DIG";

            upgrade_button = new Button(
                upgrade_button_position,
                upgrade_command,
                upgrade_button_name,
                button_assets);

            Command throwOutLoot_command = new Command(lair_obj.remove_loot);
            throwOutLoot_button = new Button(
                new Vector2(1000, 630),
                throwOutLoot_command,
                "LOOT",
                button_assets);
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            font_40 = Content.Load<SpriteFont>("Info_40");
            font_30 = Content.Load<SpriteFont>("Info_30");

            background_img = Content.Load<Texture2D>("survival_background");
            outside_img = Content.Load<Texture2D>("forest");
            storage_img = Content.Load<Texture2D>("storage");

            //LEVEL textures
            level_1_texture = Content.Load<Texture2D>("level_1");
            level_2_texture = Content.Load<Texture2D>("level_2");
            level_3_texture = Content.Load<Texture2D>("level_3");
            level_4_texture = Content.Load<Texture2D>("level_4");
            level_5_texture = Content.Load<Texture2D>("level_5");
            level_6_texture = Content.Load<Texture2D>("level_6");
            level_7_texture = Content.Load<Texture2D>("level_7");
            level_8_texture = Content.Load<Texture2D>("level_8");
            level_9_texture = Content.Load<Texture2D>("level_9");
            
            //BUTTON textures
            button_texture = Content.Load<Texture2D>("button");
            button_hover_texture = Content.Load<Texture2D>("button_hover");
            button_pressed_texture = Content.Load<Texture2D>("button_pressed");
            button_inactive_texture = Content.Load<Texture2D>("button_inactive");
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        protected override void Update(GameTime gameTime)
        {
            // TODO: Add your update logic here
            GraphicsDevice.Clear(Color.Black);
            if (current_screen == ScreenType.Lair)
            {
                Lair_screen(gameTime);
            }
            else if (current_screen == ScreenType.Outside)
            {
                Outside_screen(gameTime);
            }
            else if (current_screen == ScreenType.Storage)
            {
                Storage_screen(gameTime);
            }
            else if (current_screen == ScreenType.Lost)
            {
                Update_Lost();
            }
            else if (current_screen == ScreenType.Victory)
            {
                Update_Victory();
            }
            
            base.Update(gameTime);
        }

        private void Update_game(GameTime gameTime)
        {
            escape_key.update();
            mouse_helper.update();

            if (player_obj.Current_nom_level == GameLogic.Player.NomLevels.Dead)
            {
                Lose_game();
            }
        }

        private void Update_Lost()
        {
            escape_key.update();

            spriteBatch.Begin();

            string lost_message = "You have starved to death!";
            Vector2 lost_message_pos = new Vector2(500, 500);
            spriteBatch.DrawString(
                font_40,
                lost_message,
                lost_message_pos,
                Color.White);

            string quit_instruction = "Press ESC to quit";
            Vector2 quit_instruction_pos = new Vector2(500, 600);
            spriteBatch.DrawString(
                font_40,
                quit_instruction,
                quit_instruction_pos,
                Color.White);

            spriteBatch.End();
        }

        private void Update_Victory()
        {
            escape_key.update();

            spriteBatch.Begin();

            string victory_message = "Your lair is the createst lair, ever! You win!";
            Vector2 victory_message_pos = new Vector2(500, 500);
            spriteBatch.DrawString(
                font_40,
                victory_message,
                victory_message_pos,
                Color.White);

            string quit_instruction = "Press ESC to quit";
            Vector2 quit_instruction_pos = new Vector2(500, 600);
            spriteBatch.DrawString(
                font_40,
                quit_instruction,
                quit_instruction_pos,
                Color.White);

            spriteBatch.End();
        }

        protected override void Draw(GameTime gameTime)
        {
            

            // TODO: Add your drawing code here

            base.Draw(gameTime);
            
        }

        private void Draw_game()
        {
            string elapsed_days_text = "Days: " + elapsed_days.ToString();
            Vector2 days_coords = new Vector2(10, 10);
            spriteBatch.DrawString(
                font_40,
                elapsed_days_text,
                days_coords,
                Color.White);

            player_obj.Draw(spriteBatch, font_40);
        }

        private void Lose_game()
        {
            current_screen = ScreenType.Lost;
        }

        private void Win_game()
        {
            current_screen = ScreenType.Victory;
        }

        private void Go_outside()
        {
            current_screen = ScreenType.Outside;
        }

        void Outside_screen(GameTime gameTime)
        {
            spriteBatch.Begin();

            Update_game(gameTime);
            hunt_button.update(mouse_helper);
            loot_button.update(mouse_helper);
            goLair_button.update(mouse_helper);
            spriteBatch = hunt_button.draw(spriteBatch);
            spriteBatch = loot_button.draw(spriteBatch);
            spriteBatch = goLair_button.draw(spriteBatch);
            spriteBatch = outside_obj.Draw(spriteBatch, font_40);

            Draw_game();

            spriteBatch.End();
        }

        private void Go_Lair()
        {
            if (current_screen == ScreenType.Outside)
            {
                double collected_loot = outside_obj.Take_Loot();
                lair_obj.add_loot(collected_loot);
            }
            
            if (lair_obj.Is_full())
            {
                current_screen = ScreenType.Storage;
            }
            else
            {
                current_screen = ScreenType.Lair;
            }
        }

        void Lair_screen(GameTime gameTime)
        {
            spriteBatch.Begin();

            Draw_game();

            Update_game(gameTime);
            upgrade_button.update(mouse_helper);
            goOut_button.update(mouse_helper);
            spriteBatch = lair_obj.draw(spriteBatch, font_40);
            spriteBatch = upgrade_button.draw(spriteBatch);
            spriteBatch = goOut_button.draw(spriteBatch);

            spriteBatch.End();
        }

        void Storage_screen(GameTime gameTime)
        {
            if (lair_obj.Is_full())
            {
                spriteBatch.Begin();

                spriteBatch.Draw(
                    storage_img,
                    new Vector2(0, 0),
                    Color.White);

                Update_game(gameTime);

                string screen_text = "Everything doesn't fit into the Lair. Get rid of some stuff.";
                spriteBatch.DrawString(
                    font_40,
                    screen_text,
                    new Vector2(300, 10),
                    Color.White);

                int storage_space = lair_obj.Level.Size;
                string storage_space_text = "Storage space: " + storage_space.ToString();
                spriteBatch.DrawString(
                    font_40,
                    storage_space_text,
                    new Vector2(600, 200),
                    Color.White);

                int used_storage = (int)lair_obj.Loot_amount;
                string used_storage_text = "Used storage space: " + used_storage.ToString();
                spriteBatch.DrawString(
                    font_40,
                    used_storage_text,
                    new Vector2(600, 300),
                    Color.White);

                int stored_loot = (int)lair_obj.Loot_amount;
                string stored_loot_text = "Loot: " + stored_loot.ToString();
                spriteBatch.DrawString(
                    font_40,
                    stored_loot_text,
                    new Vector2(600, 400),
                    Color.White);

                string throw_away_text = "Throw away:";
                spriteBatch.DrawString(
                    font_40,
                    throw_away_text,
                    new Vector2(600, 600),
                    Color.White);

                throwOutLoot_button.update(mouse_helper);
                throwOutLoot_button.draw(spriteBatch);

                Draw_game();

                spriteBatch.End();
            }
            else
            {
                current_screen = ScreenType.Lair;
            }
        }

        private void Advance_time()
        {
            elapsed_days = elapsed_days + 1;
            player_obj.AdvanceTime();
            if (current_screen == ScreenType.Lair)
            {
                lair_obj.update();
            }
        }
    }

    public class Button
    {
        Texture2D texture;
        Texture2D hover_texture;
        Texture2D pressed_texture;
        Texture2D inactive_texture;
        Vector2 size;
        Vector2 position;
        Command command;
        string name;
        enum StateType
        {
            Default,
            Hover,
            Pressed,
            Inactive
        }
        StateType state;
        SpriteFont font;

        public Button(
            Vector2 button_position,
            Command button_command,
            string button_name,
            ButtonAssets button_assets)
        {
            texture = button_assets.texture;
            hover_texture = button_assets.hover_texture;
            pressed_texture = button_assets.pressed_texture;
            inactive_texture = button_assets.inactive_texture;
            size = button_assets.size;
            position = button_position;
            command = button_command;
            name = button_name;
            state = StateType.Default;
            font = button_assets.font;
        }

        public SpriteBatch draw(SpriteBatch spriteBatch)
        {
            Texture2D draw_texture;
            if (state == StateType.Hover)
            {
                draw_texture = hover_texture;
            }
            else if (state == StateType.Pressed)
            {
                draw_texture = pressed_texture;
            }
            else
            {
                draw_texture = texture;
            }
            spriteBatch.Draw(
                draw_texture,
                position,
                Color.White);

            Vector2 text_pos = new Vector2((position.X + 10), (position.Y + 10));
            spriteBatch.DrawString(
                font,
                name,
                text_pos,
                Color.White);

            return spriteBatch;
        }

        public void update(MouseHelper mouse_helper)
        {
            if (state != StateType.Inactive)
            {
                if (pressed())
                {
                    state = StateType.Pressed;
                    if (mouse_helper.Left_button_event)
                    {
                        execute();
                    }
                }
                else if (hovering())
                {
                    state = StateType.Hover;
                }
                else
                {
                    state = StateType.Default;
                }
            }
        }

        public bool hovering()
        {
            bool result = false;
            Point mouse_point = Mouse.GetState().Position;

            if ((mouse_point.X > position.X) &&
                (mouse_point.X < (position.X + size.X)) &&
                (mouse_point.Y > position.Y) &&
                (mouse_point.Y < (position.Y + size.Y)))
            {
                result = true;
            }
            
            return result;
        }

        private bool pressed()
        {
            bool result = false;
            if (hovering())
            {
                if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                {
                    result = true;
                }
            }
            return result;
        }

        public void execute()
        {
            command.execute();
        }

        public void ToggleActive()
        {
            if (state == StateType.Inactive)
            {
                state = StateType.Default;
            }
            else
            {
                state = StateType.Inactive;
            }
        }
    }

    public class MouseHelper
    {
        Vector2 pos;
        private bool left_button_event;
        bool waiting_left_up;

        public MouseHelper()
        {
            Point point = Mouse.GetState().Position;
            pos = new Vector2(point.X, point.Y);
            left_button_event = false;
            waiting_left_up = false;
        }

        public bool Left_button_event { get => left_button_event; }

        public void update()
        {
            left_button_event = false;

            if (Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                if (waiting_left_up == false)
                {
                    left_button_event = true;
                }
                waiting_left_up = true;
            }
            else
            {
                waiting_left_up = false;
            }
        }
    }

    public class Key
    {
        Keys name;
        private bool press_event;
        bool waiting_up;
        Command command;

        public Key(Keys key_name, Command key_command)
        {
            name = key_name;
            press_event = false;
            waiting_up = false;
            command = key_command;
        }

        public bool Press_event { get => press_event; set => press_event = value; }

        public void update()
        {
            press_event = false;
            if (Keyboard.GetState().IsKeyDown(name))
            {
                if (waiting_up == false)
                {
                    press_event = true;
                    waiting_up = true;
                }
            }
            else
            {
                waiting_up = false;
            }

            if (press_event == true)
            {
                execute();
            }
        }

        private void execute()
        {
            command.execute();
        }
    }

    public class Command
    {
        private Action func;

        public Command(Action command_func)
        {
            func = command_func;
        }

        public void execute()
        {
            func();
        }
    }
    
    public class ButtonAssets
    {
        public Texture2D texture;
        public Texture2D hover_texture;
        public Texture2D pressed_texture;
        public Texture2D inactive_texture;
        public Vector2 size;
        public SpriteFont font;

        public ButtonAssets(
            Texture2D button_texture, 
            Texture2D button_hover_texture,
            Texture2D button_pressed_texture,
            Texture2D button_inactive_texture,
            Vector2 button_size,
            SpriteFont button_font)
        {
            texture = button_texture;
            hover_texture = button_hover_texture;
            pressed_texture = button_pressed_texture;
            inactive_texture = button_inactive_texture;
            size = button_size;
            font = button_font;
        }
    }
}
